from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from flask_sqlalchemy_session import flask_scoped_session

engine = create_engine('sqlite:///test.db',
                       convert_unicode=True)
session_factory = sessionmaker(autocommit=False,
                               autoflush=False,
                               bind=engine)


def make_session(app):
    db_session = flask_scoped_session(session_factory, app)
    return db_session


Base = declarative_base()


def init_db():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    # import models
    Base.metadata.create_all(bind=engine)
