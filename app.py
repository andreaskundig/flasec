import os

from flask import Flask, render_template_string
from flask_security import Security, current_user, auth_required
from flask_security import hash_password, SQLAlchemySessionUserDatastore
from database import make_session, init_db
from models import Base, User, Role

# Create app
app = Flask(__name__)
app.config['DEBUG'] = True

# Generate a nice key using secrets.token_urlsafe()
app.config['SECRET_KEY'] = os.environ.get(
    "SECRET_KEY",
    'pf9Wkove4IKEAXvy-cQkeDPhv9Cb3Ag-wyJILbq_dFw')
# Bcrypt is set as default SECURITY_PASSWORD_HASH, which requires a salt
# Generate a good salt using: secrets.SystemRandom().getrandbits(128)
app.config['SECURITY_PASSWORD_SALT'] = os.environ.get(
    "SECURITY_PASSWORD_SALT",
    '146585145368132386173505678016728509634')


# https://flask-security-too.readthedocs.io/en/stable/spa.html
def configure_spa(app):
    import flask_wtf
    app.config.update(
        # no forms so no concept of flashing
        SECURITY_FLASH_MESSAGES=False,

        # Need to be able to route backend flask API calls. Use 'accounts'
        # to be the Flask-Security endpoints.
        SECURITY_URL_PREFIX='/api/accounts',

        # Turn on all the great Flask-Security features
        SECURITY_RECOVERABLE=True,
        SECURITY_TRACKABLE=True,
        SECURITY_CHANGEABLE=True,
        # SECURITY_CONFIRMABLE=True,
        SECURITY_REGISTERABLE=True,
        # SECURITY_UNIFIED_SIGNIN=True,

        SECURITY_SEND_REGISTER_EMAIL=False,
        SECURITY_SEND_PASSWORD_CHANGE_EMAIL=False,

        # These need to be defined to handle redirects
        # As defined in the API documentation
        # - they will receive the relevant context
        # SECURITY_POST_CONFIRM_VIEW="/confirmed",
        # SECURITY_CONFIRM_ERROR_VIEW="/confirm-error",
        # this is used by RECOVERABLE
        SECURITY_RESET_VIEW="/reset-password",
        SECURITY_RESET_ERROR_VIEW="/reset-password",
        SECURITY_REDIRECT_BEHAVIOR="spa",
        
        # CSRF protection is critical for all session-based browser UIs
        
        # enforce CSRF protection for session / browser - but allow token-based
        # API calls to go through
        SECURITY_CSRF_PROTECT_MECHANISMS=["session", "basic"],
        SECURITY_CSRF_IGNORE_UNAUTH_ENDPOINTS=True,

        # Send Cookie with csrf-token. This is the default for Axios and Angular.
        SECURITY_CSRF_COOKIE={"key": "XSRF-TOKEN"},
        WTF_CSRF_CHECK_DEFAULT=False,
        WTF_CSRF_TIME_LIMIT=None,

        # https://flask-security-too.readthedocs.io/en/stable/patterns.html#csrf
        # You can't get the cookie until you are logged in.
        # SECURITY_CSRF_IGNORE_UNAUTH_ENDPOINTS=True,
    )

    flask_wtf.CSRFProtect(app)


configure_spa(app)

db_session = make_session(app)
Base.query = db_session.query_property()

# Setup Flask-Security
user_datastore = SQLAlchemySessionUserDatastore(db_session, User, Role)
security = Security(app, user_datastore)


def unauthorized(func, params):
    return 'unauthorized'


# security.unauthz_handler(unauthorized)


# Create a user to test with
@app.before_first_request
def create_user():
    init_db()
    user = user_datastore.find_user(email="test@me.com")
    if not user:
        user_datastore.create_user(email="test@me.com",
                                   password=hash_password("password"))
        db_session.commit()


# Views
@app.route("/")
@auth_required()
def home():
    return render_template_string('Hello {{email}} !',
                                  email=current_user.email)


@app.route("/api/accounts/confirmed")
def confirmed():
    return 'Hello confirmed '


@app.route("/api/accounts/confirm-error")
def confirm_error():
    return 'confirm error '


@app.route("/api/accounts/reset-password")
def reset_password():
    return 'reset password'


@app.route("/api/hello")
@auth_required()
def hello():
    return 'Hello hello'


if __name__ == '__main__':
    app.run(port=5001)
